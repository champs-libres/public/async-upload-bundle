<?php

namespace ChampsLibres\AsyncUploaderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use ChampsLibres\AsyncUploaderBundle\TempUrl\TempUrlGeneratorException;
use ChampsLibres\AsyncUploaderBundle\TempUrl\TempUrlGeneratorInterface;

/**
 * Class TempURLController
 *
 * @package ChampsLibres\AsyncUploaderBundle\Controller
 */
class TempURLController extends AbstractController
{
    /**
     * @var TempUrlGeneratorInterface
     */
    protected $tempUrlGenerator;
    
    /**
     * TempURLController constructor.
     *
     * @param TempUrlGeneratorInterface $tempUrlGenerator
     */
    public function __construct(TempUrlGeneratorInterface $tempUrlGenerator)
    {
        $this->tempUrlGenerator = $tempUrlGenerator;
    }
    
    /**
     * @param Request $request
     * @Route("/asyncupload/temp_url/generate/{method}", 
     *      name="async_upload.generate_url")
     */
    public function getUrlAction($method, Request $request)
    {
        try {
            $temp_url_generator = $this->tempUrlGenerator;
            
            switch ($method) {
                case 'post':
                    $p = $temp_url_generator
                        ->generatePost(
                            $request->query->getInt('expires_delay', 0), // sf4 check: default value to 0, ok ?
                            $request->query->getInt('submit_delay', 0)
                            )
                        ;
                    break;
                case 'get':
                case 'GET':
                    $object_name = $request->query->get('object_name', null);
                    
                    if ($object_name === null) {
                        return (new JsonResponse((object) [ 
                            'message' => "the object_name is null" 
                            ]))
                            ->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
                    }
                    $p = $temp_url_generator->generate(
                            'GET',
                            $object_name, 
                            $request->query->has('expires_delay') ? $request->query->getInt('expires_delay', 0) : null
                            );
                    break;
                default:
                    return (new JsonResponse((object) [ 'message' => "the method "
                        . "$method is not valid"]))
                        ->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            }
            
            
        } catch (TempUrlGeneratorException $e) {
            
            $this->get('logger')->warning("The client requested a temp url"
                . " which sparkle an error.", [
                    'message' => $e->getMessage(),
                    'expire_delay' => $request->query->getInt('expire_delay', 0),
                    'file_count' => $request->query->getInt('file_count', 1)
                ]);
            
            $p = new \stdClass;
            $p->message = $e->getMessage();
            $p->status = JsonResponse::HTTP_BAD_REQUEST;
            
            return (new JsonResponse($p))
                ->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
        }
        
        return new JsonResponse($p);
    }
    
 
}
