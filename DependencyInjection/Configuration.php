<?php

namespace ChampsLibres\AsyncUploaderBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('champs_libres_async_uploader');
        $rootNode = $treeBuilder->getRootNode('champs_libres_async_uploader');

        $rootNode
            ->children()
            ->arrayNode('openstack')
                ->info("parameters to connect to the openstack swift repository")
                ->children()
                    ->scalarNode('os_username')
                        ->isRequired()->cannotBeEmpty()
                    ->end()
                    ->scalarNode('os_password')
                        ->isRequired()->cannotBeEmpty()
                    ->end()
                    ->scalarNode('os_tenant_id')
                        ->isRequired()->cannotBeEmpty()
                     ->end()
                    ->scalarNode('os_region_name')
                        ->isRequired()->cannotBeEmpty()
                    ->end()
                    ->scalarNode('os_auth_url')
                        ->isRequired()->cannotBeEmpty()
                    ->end()
                ->end()
            ->end() // end array openstack
            ->arrayNode('temp_url')
                ->children()
                    ->scalarNode('temp_url_key')
                        ->isRequired()->cannotBeEmpty()
                        ->info('the temp url key')
                    ->end()
                    ->scalarNode('temp_url_base_path')
                        ->isRequired()->cannotBeEmpty()
                        ->info('the base path to add **before** the path to media. Should '
                            . 'terminate with a "/"')
                    ->end()
                    ->scalarNode('container')
                        ->isRequired()->cannotBeEmpty()
                    ->end()
                    ->integerNode('max_post_file_size')
                        ->isRequired()
                    ->end()
                    ->integerNode('max_expires_delay')
                        ->isRequired()
                        ->info('the maximum of seconds a cryptographic signature '
                            . 'will be valid for submitting a file. This should be '
                            . 'short, to avoid uploading multiple files')
                    ->end()
                    ->integerNode('max_submit_delay')
                        ->isRequired()
                        ->info('the maximum of seconds between the upload of a file and '
                            . 'a the submission of the form. This delay will also prevent '
                            . 'the check of persistence of uploaded file. Should be long '
                            . 'enough for keeping user-friendly forms')
                    ->end()
                ->end()
            ->end() // end array temp_url
            ->scalarNode('persistence_checker')
                ->info('A service to check the persistence of the entity. This service '
                    . 'must implements Champs-Libres\\AsyncUploadBundle\\Persistence\\PersistenceCheckerInterface '
                    . '(currently unused)')
                ->defaultNull()
                ->setDeprecated("deprecated since we removed the dependency to RabbitMQ")
            ->end()
            ;

        return $treeBuilder;
    }
}
