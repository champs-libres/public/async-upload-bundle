<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\TempUrl;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TempUrlGeneratorException extends \RuntimeException
{
}
