<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use ChampsLibres\AsyncUploaderBundle\Model\AsyncFileInterface;
use ChampsLibres\AsyncUploaderBundle\Validator\Constraints\AsyncFileExists;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use ChampsLibres\AsyncUploaderBundle\Form\DataTransformer\JsonToModelDataTransformer;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use ChampsLibres\AsyncUploaderBundle\TempUrl\TempUrlGeneratorInterface;


/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class AsyncUploaderType extends AbstractType
{
    /**
     *
     * @var UrlGeneratorInterface
     */
    private $url_generator;
    
    /**
     *
     * @var TempUrlGeneratorInterface
     */
    private $temp_url_generator;
    
    /**
     *
     * @var integer
     */
    private $expires_delay;
    
    /**
     *
     * @var integer
     */
    private $max_submit_delay;
    
    /**
     *
     * @var integer
     */
    private $max_post_size;
    
    public function __construct(
        TempUrlGeneratorInterface $temp_url_generator,
        UrlGeneratorInterface $url_generator,
        $expires_delay,
        $max_submit_delay,
        $max_post_size
    ) {
        $this->url_generator = $url_generator;
        $this->temp_url_generator = $temp_url_generator;
        $this->expires_delay = $expires_delay;
        $this->max_post_size = $max_post_size;
        $this->max_submit_delay = $max_submit_delay;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'expires_delay' => $this->expires_delay,
            'max_post_size' => $this->max_post_size,
            'submit_delay'  => $this->max_submit_delay,
            'max_files' => 1,
            'error_bubbling' => false
        ]);
        
        $resolver->setAllowedTypes('expires_delay', ['int']);
        $resolver->setAllowedTypes('max_post_size', ['int']);
        $resolver->setAllowedTypes('max_files', ['int']);
        $resolver->setAllowedTypes('submit_delay', ['int']);
    }
    
    public function buildView(
        FormView $view, 
        FormInterface $form, 
        array $options
    ) {
        $view->vars['attr']['data-async-file-upload'] = true;
        $view->vars['attr']['data-generate-temp-url-post'] = $this
            ->url_generator->generate('async_upload.generate_url', [
                'expires_delay' => $options['expires_delay'],
                'method' => 'post',
                'submit_delay' => $options['submit_delay']
            ]);
        $view->vars['attr']['data-temp-url-get'] = $this->url_generator
            ->generate('async_upload.generate_url', [ 'method' => 'GET'] );
        $view->vars['attr']['data-max-files'] = $options['max_files'];
        $view->vars['attr']['data-max-post-size'] = $options['max_post_size'];
    }
    
    public function getParent()
    {
        return HiddenType::class;
    }
    
    public function getBlockPrefix()
    {
        return 'async_uploader';
    }
}
