<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\TempUrl;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use ChampsLibres\AsyncUploaderBundle\Openstack\Client;
use Psr\Log\LoggerInterface;
use ChampsLibres\AsyncUploaderBundle\Persistence\PersistenceCheckerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use ChampsLibres\AsyncUploaderBundle\Event\ObjectEvent;

/**
 * 
 *
 * @deprecated This class is not executed since dependency to AMQPMessage is removed.
 *     We keep it only for reference for a better implementation of the existence
 *     of an object into the store.
 * 
 */
class AsyncUploaderSignatureChecker implements ConsumerInterface
{
        /**
     *
     * @var Client
     */
    private $openstackClient;
    
    /**
     *
     * @var string
     */
    private $containerName;
    
    /**
     *
     * @var LoggerInterface
     */
    private $logger;
    
    /**
     *
     * @var PersistenceCheckerInterface
     */
    private $persistenceChecker;
    
    /**
     *
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    
    /**
     *
     * @var boolean
     */
    private $isDebug;
    
    public function __construct(
        PersistenceCheckerInterface $persistenceChecker,
        Client $openstackClient, 
        $containerName,
        LoggerInterface $logger,
        EventDispatcherInterface $dispatcher,
        $isDebug
    ) {
        $this->openstackClient = $openstackClient;
        $this->containerName = $containerName;
        $this->logger = $logger;
        $this->persistenceChecker = $persistenceChecker;
        $this->eventDispatcher = $dispatcher;
        $this->isDebug = $isDebug;
    }
    
    public function execute(AMQPMessage $msg)
    {
        $generated = \unserialize($msg->getBody());
        
        $this->logger->info('handling message for generated signature', 
            (array) $generated);
        
        if ($generated->expires > (int) (new \DateTime('now'))->format('U')) {
            $this->logger->debug('generated signature still not yet expired');
            
            return false;
        }
        
        $container = $this->openstackClient
            ->objectStoreV1()
            ->getContainer($this->containerName)
            ;
        
        $counter = 0;
        foreach ($container->listObjects(['prefix' => $generated->prefix]) as $obj) {
            if (substr($obj->name, 0, strlen($generated->prefix)) !== $generated->prefix) {
                
                $this->logger->error("The object does not start with "
                    . "the expected prefix, there should be an error somewhere")
                    ;
                
                return false;
            }
            $this->checkObject($obj);
            $counter++;
        }
        
        $this->logger->info('checked objects for a given prefix', [
            'found' => $counter,
            'prefix' => $generated->prefix
        ]);
        
        return true;
    }
    
    protected function checkObject($object)
    {
        if ($this->persistenceChecker->isPersisted($object->name)) {
            $this->eventDispatcher->dispatch(
                ObjectEvent::PERSISTED, 
                new ObjectEvent($object->name)
                );
        } else {
            $this->logger->info("Removing an object which was not persisted", [
                'object_name' => $object->name
            ]);
                
            // due to https://github.com/php-opencloud/openstack/issues/163 whe 
            // have to retrieve the object again
            $object = $this->openstackClient
                ->objectStoreV1()
                ->getContainer($this->containerName)
                ->getObject($object->name)
                ;
            
            $object->delete();
        }
    }
}
