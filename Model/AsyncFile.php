<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\Model;

/**
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class AsyncFile implements \JsonSerializable, AsyncFileInterface
{
    /**
     *
     * @var string
     */
    private $object_name;
    
    public function __construct($object_name)
    {
        $this->object_name = $object_name;
    }
    
    public function __toString() {
        return $this->getObjectName();
    }
    
    public function getObjectName()
    {
        return $this->object_name;
    }

    public function setObjectName($object_name)
    {
        $this->object_name = $object_name;
        
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'object_name' => $this->getObjectName()
        ];
    }
}
